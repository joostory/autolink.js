function autolink(basedom) {
	travel(basedom, _skip_autolink, _autolink);
}

function _skip_autolink(nodeName) {
	var i,
		skipnode = ["META", "SCRIPT", "STYLE", "A", "HEAD", "IMG"];
	for (i = 0 ; i < skipnode.length ; i++) {
		if (skipnode[i] == nodeName) {
			return true;
		}
	}
	return false;
}

function _autolink(textNode) {
	var pattern = /(^|\s|\()((?:https?|ftp):\/\/[\-A-Z0-9+\u0026@#\/%?=~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~_|])/gi;
	if (textNode.data.match(pattern)) {
		travel_replace(textNode, _replace_link);
	}
}

function _replace_link(text) {
	return text.replace(/(^|\s|\()((?:https?|ftp):\/\/[\-A-Z0-9+\u0026@#\/%?=~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~_|])/gi, "$1<a href='$2'>$2</a>");
}
