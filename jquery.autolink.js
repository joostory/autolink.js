(function($) {

	var PATTERN = /(^|\s|\()((?:https?|ftp):\/\/[\-A-Z0-9+\u0026@#\/%?=~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~_|])/gi,
		SKIPNODE = ["META", "SCRIPT", "STYLE", "A", "HEAD", "IMG"];

	function _findTextNode(dom) {
		var i, contents = [];

		$(dom).contents().each(function() {
			if ($.inArray(this.nodeName, SKIPNODE) > -1) {
				return;
			}

			if (this.nodeType != Node.TEXT_NODE) {
				contents = contents.concat(_findTextNode(this));
			} else {
				contents.push(this);
			}
		});
		return contents;
	}

	function _escapeHTML (unsafe_str) {
		return unsafe_str
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;'); // '&apos;' is not valid HTML 4
	}


	function autolink(dom) {
		var contents = _findTextNode(dom);
		$.each(contents, function() {
			var node = this, text = _escapeHTML(this.data);
			if (text.match(PATTERN)) {
				var temp = document.createElement("span");
			    temp.innerHTML = text.replace(PATTERN, "$1<a href='$2'>$2</a>");

			    while (temp.firstChild) {
			        node.parentNode.insertBefore(temp.firstChild, node);
			    }
			    node.parentNode.removeChild(node);
			}
		});
	}


	$.fn.autolink = function() {
		autolink(this);
		return this;
	};

})(jQuery);
