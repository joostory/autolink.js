# autolink
Dom을 탐색하여 url에 링크를 건다. ([travel.js](https://bitbucket.org/joostory/travel.js) 사용)

# 사용법
	autolink(dom);

# 사용예
	<!doctype html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>Auto link</title>
	</head>
	<body>
		<h1>Auto link</h1>

		This is a link to Google <a href='http://google.com' target='_blank' rel='nofollow' id='1'>http://google.com</a>
		<p style="background: url(http://example.com/logo.png) ">This is a link to image http://example.com/logo.png</p>

		<script src="travel.js"></script>
		<script src="autolink.js"></script>
		<script>
			autolink(document.body);
		</script>
	</body>
	</html>
